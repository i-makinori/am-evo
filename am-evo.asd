
(in-package :cl-user)
(defpackage am-evo-asd
  (:use :cl :asdf))
(in-package :am-evo-asd)

(defsystem am-evo
   :name "am-evo"
  :author "makinori"
  :version "1"
  :maintainer ""
  :licence "MIT"
  :description "am-evo: Simulation for EVOlution at the Artificial Mundus"
  :long-description ""
  :depends-on (:bordeaux-threads
               :usocket
               :i-math
               :sdl2 
               :cl-opengl ;; #:cl-cairo2
               )
  :components
  ((:file "package")
   (:module "src"
            :components
            ((:file "utility")
             (:file "buffer-object")
             (:file "test")
             ))))



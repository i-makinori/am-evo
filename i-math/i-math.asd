
(in-package :cl-user)
(defpackage i-math-asd
  (:use :cl :asdf))
(in-package :i-math-asd)

(defsystem i-math
   :name "i-math"
  :author "i-makinori"
  :version "1"
  :maintainer ""
  :licence "MIT"
  :description "am-evo: Simulation for EVOlution at the Artificial Mundus"
  :long-description ""
  :depends-on ()
  :components
  ((:file "package")
   (:file "vector")
   ))




